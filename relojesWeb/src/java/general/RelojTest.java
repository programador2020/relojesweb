package general;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class RelojTest {
    public static void main(String[] args) {
        System.out.println("Hola");
        
        //Ejemplo de conexion a base de datos ficticia
        DbRelojes miBaseDeDatos = new DbRelojes();
        System.out.println(miBaseDeDatos.consulta());
        
        //Ejemplo de conexion a base de datos real usan singleton
        Connection miConexion = null;
        try {
            
            miConexion = DB.getInstance().getConnection();
            //2.Preparar la consulta SQL
            String consultaSQL = "SELECT * FROM materias";
            PreparedStatement sqlPreparado = miConexion.prepareStatement(consultaSQL);

            //3  Ejecutar la consulta SQL
            ResultSet miResultado = sqlPreparado.executeQuery();
            //4. Mostrar los resultados
            while (miResultado.next()) {
                System.out.println(miResultado.getString("horario"));
            }

            System.out.println("Se va ejecutando bien...");
        } catch (ClassNotFoundException ex) {
            System.out.println(ex);
        } catch (IOException ex) {
            System.out.println(ex);
        } catch (SQLException ex) {
            System.out.println(ex);
        }finally {
            if (miConexion != null){
                try {
                    miConexion.close();
                } catch (SQLException ex) {
                    System.out.println("Error al cerrar la base de datos.");
                    System.out.println(ex);
                }
            }
            
        }
        
        System.out.println("Chau");
    }
}
