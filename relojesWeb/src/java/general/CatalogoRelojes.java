package general;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "CatalogoRelojes", urlPatterns = {"/CatalogoRelojes"})
public class CatalogoRelojes extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            DbRelojes miBaseDeDatos = new DbRelojes();
//            out.println("<p>" + miBaseDeDatos.consulta().get(0) + "</p>");
//            out.println("<p>" + miBaseDeDatos.consulta().get(1) + "</p>");
//            out.println("<p>" + miBaseDeDatos.consulta().get(2) + "</p>");

            //Ejemplo de conexion a base de datos real usan singleton
            Connection miConexion = null;
            try {

                miConexion = DB.getInstance().getConnection();
                //2.Preparar la consulta SQL
                String consultaSQL = "SELECT * FROM materias";
                PreparedStatement sqlPreparado = miConexion.prepareStatement(consultaSQL);

                //3  Ejecutar la consulta SQL
                ResultSet miResultado = sqlPreparado.executeQuery();
                //4. Mostrar los resultados
                while (miResultado.next()) {
                    System.out.println(miResultado.getString("horario"));
                    out.println("<p>"+ miResultado.getString("horario") +"</p>");
                }

                System.out.println("Se va ejecutando bien...");
            } catch (ClassNotFoundException ex) {
                System.out.println(ex);
            } catch (IOException ex) {
                System.out.println(ex);
            } catch (SQLException ex) {
                System.out.println(ex);
            } finally {
                if (miConexion != null) {
                    try {
                        miConexion.close();
                    } catch (SQLException ex) {
                        System.out.println("Error al cerrar la base de datos.");
                        System.out.println(ex);
                    }
                }

            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
