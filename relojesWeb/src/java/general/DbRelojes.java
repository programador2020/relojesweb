package general;

import java.util.ArrayList;

//Simulamos tener nuestra base de datos
public class DbRelojes {
    ArrayList consulta() {
        ArrayList listado = new ArrayList();
        Reloj miReloj1 = new Reloj();
        miReloj1.nombre = "marca 1";
        miReloj1.precio = 10000;
        Reloj miReloj2 = new Reloj();
        miReloj2.nombre = "marca 2";
        miReloj2.precio = 25000;
        Reloj miReloj3 = new Reloj();
        miReloj3.nombre = "marca 3";
        miReloj3.precio = 15000;
        
        listado.add(miReloj1);
        listado.add(miReloj2);
        listado.add(miReloj3);
        
        return listado;
    }
}
